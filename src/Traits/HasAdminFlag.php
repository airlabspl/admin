<?php

namespace Airlabs\Admin\Traits;

trait HasAdminFlag
{
    public function isAdmin()
    {
        $column = config('admin.column', 'is_admin');

        return true === !! $this->$column;
    }
}
