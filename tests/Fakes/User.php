<?php

namespace Tests\Fakes;

use Airlabs\Admin\Traits\HasAdminFlag;

class User
{
    use HasAdminFlag;

    public $is_admin = false;

    public $admin_flag = false;
}
