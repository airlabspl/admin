<?php

namespace Airlabs\Admin;

use Airlabs\Admin\Commands\CreateAdmin;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->config();
        $this->migrations();
        $this->publishing();
        $this->console();
        $this->blade();
    }

    protected function config()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/admin.php', 'admin');
    }

    protected function migrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    protected function publishing()
    {
        $this->publishes([
            __DIR__ . '/../config/admin.php' => config_path('admin.php')
        ], 'config');
    }

    protected function console()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateAdmin::class
            ]);
        }
    }

    protected function blade()
    {
        Blade::if('admin', function () {
            return auth()->check() && auth()->user()->isAdmin();
        });
    }
}
